class HuffData:

    def __init__(self, symbol, frequency=0, huff_code=None):
        self.symbol = symbol
        self.frequency = frequency
        self.huff_code = huff_code

    def __eq__(self, other):
        return self.frequency == other.frequency

    def __ne__(self, other):
        return self.frequency != other.frequency

    def __gt__(self, other):
        return self.frequency > other.frequency

    def __ge__(self, other):
        return self.frequency >= other.frequency

    def __lt__(self, other):
        return self.frequency < other.frequency

    def __le__(self, other):
        return self.frequency <= other.frequency

    def __str__(self):
        return f'<symbol={str(self.symbol)}, frequency= {str(self.frequency)}, ' \
               f'huff_code={str(self.huff_code)}'

    __repr__ = __str__