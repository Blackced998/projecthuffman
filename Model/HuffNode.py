class HuffNode:

    def __init__(self, symbol=None, frequency=None, left=None, right=None):
        self.symbol = symbol
        self.frequency = frequency
        self.left_child = left
        self.right_child = right

    def is_leaf(self):
        if self.right_child is None and self.left_child is None:
            return 1
        else:
            return 0

    def __eq__(self, other):
        return self.frequency == other.frequency

    def __ne__(self, other):
        return self.frequency != other.frequency

    def __gt__(self, other):
        return self.frequency > other.frequency

    def __ge__(self, other):
        return self.frequency >= other.frequency

    def __lt__(self, other):
        return self.frequency < other.frequency

    def __le__(self, other):
        return self.frequency <= other.frequency

    def __str__(self):
        return f'(symbol={self.symbol}, frequency= {str(self.frequency)})'

    __repr__ = __str__
