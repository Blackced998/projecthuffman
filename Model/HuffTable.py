from Model.HuffData import HuffData
from Util.GenericCollections.SeparateChainingHashtable import SeparateChainingHashtable
from Util.GenericCollections.ArrayList import ArrayList

class HuffTable:
    def __init__(self, list_of_char=None, list_of_huff_data = None):
        self.list_of_char: ArrayList = list_of_char
        self.huff_table: SeparateChainingHashtable = SeparateChainingHashtable()
        self.list_of_huff_data = list_of_huff_data


    def build_huff_data_map(self ):
        print("creating huffman table")
        if self.list_of_char is not None:
            for i in range(0, self.list_of_char.__len__(), 1):
                current = self.list_of_char[i]
                if self.huff_table.get(current):
                    self.huff_table.get(current).frequency += 1
                else:
                    self.huff_table.put(current, HuffData(current, 1))
        else:
            for i in range(0,self.list_of_huff_data.__len__(), 1):
                e = self.list_of_huff_data[i]
                self.huff_table.put(e.symbol, HuffData(e.symbol, e.frequency, e.huff_code))

        return self.huff_table





