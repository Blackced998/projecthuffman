from Model.HuffNode import HuffNode
from Util.GenericCollections.SeparateChainingHashtable import SeparateChainingHashtable
from Util.GenericCollections.LinkedBasedBinaryTree import LinkedBinaryTree
from Util.GenericCollections.SortedList import SortedList
from Util.GenericCollections.ArrayQueue import ArrayQueue


class HuffTree:

    def __init__(self, data):
        self.huff_hashtable: SeparateChainingHashtable = data
        self.huff_tree: LinkedBinaryTree = LinkedBinaryTree()

    def build_huff_tree(self):
        print("building binary tree...")
        sorted_node_list: SortedList = self.create_sorted_data(self.huff_hashtable)
        while sorted_node_list.size() > 1:
            internal_value = sorted_node_list.get(sorted_node_list.size()-1).frequency + \
                             sorted_node_list.get(sorted_node_list.size()-2).frequency
            new_internal_node = HuffNode(None, internal_value,
                                         sorted_node_list.remove_back(),
                                         sorted_node_list.remove_back())
            sorted_node_list.add(new_internal_node)

        self.huff_tree.add_root(sorted_node_list.remove_back())
        self.insert_binarycode_with_df()

    def build_huff_tree_from_huff_code(self):
        root = HuffNode("root")
        data:SeparateChainingHashtable = self.huff_hashtable
        for key in data.keys():
            huff_code: str = data.get(key).huff_code
            self.put_symbol_at_position(root,data.get(key).symbol, huff_code )
        self.huff_tree.add_root(root)

    def put_symbol_at_position(self,root, symbol, huff_code):
        if huff_code != "":
            if huff_code[:1] == "0":
                if root.left_child is None:
                    root.left_child = HuffNode("child")
                huff_code =huff_code[1:]
                self.put_symbol_at_position(root.left_child,symbol, huff_code)
            else:
                if root.right_child is None:
                    root.right_child = HuffNode("child")
                huff_code =huff_code[1:]
                self.put_symbol_at_position(root.right_child, symbol, huff_code)
        else:

            root.symbol = symbol

    def convert_binary_code(self, binary_code):
        print("converting to text")
        text: str =""


        root = self.huff_tree.root()

        while binary_code != "":


            if binary_code[:1] == "0":
                binary_code = binary_code[1:]
                root = root.left_child
            else:
                binary_code = binary_code[1:]
                root = root.right_child
            if root.is_leaf():

                text += root.symbol
                root = self.huff_tree.root()

        return text









    def create_sorted_data(self, data: SeparateChainingHashtable):
        sorted_node_list: SortedList = SortedList()
        for key in data.keys():
            new_huff_node = HuffNode(data.get(key).symbol, data.get(key).frequency)
            sorted_node_list.add(new_huff_node)
        return sorted_node_list

    def walk_bf(self):
        levelwise_walk = []
        if self.huff_tree.root() is None:
            return levelwise_walk
        to_visit_queue:ArrayQueue = ArrayQueue()
        to_visit_queue.enqueue(self.huff_tree.root())

        while to_visit_queue.empty() is False:
            e: HuffNode = to_visit_queue.dequeue()
            if e:
                levelwise_walk.append(e)
                to_visit_queue.enqueue(e.right_child)
                to_visit_queue.enqueue(e.left_child)
        return levelwise_walk

    def insert_binarycode_with_df(self):

        self.__dept_first(self.huff_tree.root())

    def __dept_first(self, root, binary_code=""):

        if root.is_leaf():
            self.huff_hashtable.get(root.symbol).huff_code = binary_code
        if root.left_child:
            binary_code += "0"
            self.__dept_first(root.left_child, binary_code)
            binary_code = binary_code[:-1]

        if root.right_child:
            binary_code += "1"
            self.__dept_first(root.right_child, binary_code)




