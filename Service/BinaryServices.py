from Model.HuffTable import HuffTable
from Util.GenericCollections.ArrayList import ArrayList


class BinaryServices:

    def __init__(self, huff_table):
        self.__huff_table: HuffTable = huff_table
        self.missing_binary_code = 0
        self.length_of_table = 0

    def convert_text_to_binary(self, iterated_text: ArrayList):
        binary_string = ""
        for i in range(0, iterated_text.length, 1):
            binary_string += self.__huff_table.get(iterated_text[i]).huff_code
        encoded_binary_string = ""
        print(f'binary code is done :{binary_string[:40]}......')
        while len(binary_string) >= 8:
            encoded_binary_string += chr(int(binary_string[:8], 2))
            binary_string = binary_string[8:]
        self.missing_binary_code = 8 - len(binary_string)
        if self.missing_binary_code != 0:
            for i in range(self.missing_binary_code):
                binary_string += "0"
            encoded_binary_string += chr(int(binary_string, 2))
        return encoded_binary_string

    def convert_table_to_char(self):
        huff_table_string = ""
        for key in self.__huff_table.keys():
            if len("1" + self.__huff_table.get(key).huff_code) > 7:

                if len("11" + self.__huff_table.get(key).huff_code) > 14:
                    separater = "ÿÿÿ"
                    string_huff_code = self.__huff_table.get(key).huff_code
                    first_part, string_huff_code = string_huff_code[:6], string_huff_code[6:]
                    middle_part, string_huff_code = string_huff_code[:6], string_huff_code[6:]
                    last_part = string_huff_code
                    huff_table_string += f'{separater}{self.__huff_table.get(key).symbol}{chr(int("1" + first_part))}{chr(int("1" + middle_part))}{chr(int("1" + last_part))}'
                else:
                    separater = "ÿÿ"
                    huff_table_string += f'{separater}{self.__huff_table.get(key).symbol}{chr(int("1" + self.__huff_table.get(key).huff_code[:6]))}{chr(int("1" + self.__huff_table.get(key).huff_code[6:]))}'
            else:
                huff_table_string += f'{self.__huff_table.get(key).symbol}{chr(int("1" + self.__huff_table.get(key).huff_code[:6]))}'

            self.length_of_table += 1
        return huff_table_string

    def get_len_missing_binary(self):
        return self.missing_binary_code

    def get_len_of_table(self):
        self.length_of_table = str(self.length_of_table)
        while len(self.length_of_table) < 3:
            self.length_of_table = "0" + self.length_of_table
        return int(self.length_of_table)


