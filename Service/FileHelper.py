from Util.GenericCollections.ArrayList import ArrayList
from Model.HuffData import HuffData
from Util.GenericCollections.ArrayQueue import ArrayQueue


class FileHelper:
    def __init__(self,filename,code=None):
        self.filename = filename
        self.file = self.__parse(code)
        self.arraylist = ArrayList()
        self.string_byte = ""


    def __parse(self, code):
        return open(self.filename, "r", encoding=code)

    def insert_data_into_list(self):
        print("parsing file...")
        if self.file.mode == "r":
            for e in self.file.read():
                self.arraylist.add_back(e)
        self.file.close()


    def fetch_symbols(self):
        queue = ArrayQueue()
        if self.file.mode == "r":

            for e in self.file.read():
                queue.enqueue(e)
        self.file.close()
        listnum = int(ord(queue.dequeue()))
        overflow_binary_code = int(ord(queue.dequeue()))
        for i in range(0, listnum, 1):
            # Ajouter dans la HashTable
            # Créer l'arbre

            if ord(queue.peek()) == 255:
                a = queue.dequeue()

                if ord(queue.peek()) == 255:
                    queue.dequeue()
                    if ord(queue.peek()) == 255:
                        queue.dequeue()
                        self.arraylist.add_back(HuffData(queue.dequeue(), huff_code=(str(ord(queue.dequeue()))[1:] + str(ord(queue.dequeue()))[1:]+ str(ord(queue.dequeue()))[1:])))
                    else:
                        self.arraylist.add_back(HuffData(queue.dequeue(),huff_code=(str(ord(queue.dequeue()))[1:]+str(ord(queue.dequeue()))[1:])))
                else:
                    print(queue.peek())

                    self.arraylist.add_back(HuffData(a,huff_code=str(ord(queue.dequeue()))[1:]))
            else:
                self.arraylist.add_back(HuffData(queue.dequeue(), huff_code=str(ord(queue.dequeue()))[1:]))
        while not queue.empty():
            full_binary = str(bin(ord(queue.dequeue())))[2:]
            while len(full_binary) < 8:
                full_binary = "0"+full_binary
            self.string_byte += full_binary
        self.string_byte = self.string_byte[:-overflow_binary_code]










    def get_sorted_list(self):
        return self.sortedList

    def get_array_list(self):
        return self.arraylist

    def get_string_byte(self):
        return self.string_byte
