from Model.HuffTable import HuffTable
from Model.HuffTree import HuffTree
from Service.FileHelper import FileHelper
from Util.GenericCollections.SortedList import SortedList
from Util.GenericCollections.ArrayList import ArrayList
from View.ViewHelper import ViewHelper
from Service.BinaryServices import BinaryServices



class HuffmanCodecEngine:

    def __init__(self, in_file, out_file):
        self.in_file = in_file
        self.out_file = open(out_file, "w+b")
        self.view_helper = ViewHelper()

    def encode(self):

        file_helper = FileHelper(self.in_file)
        file_helper.insert_data_into_list()
        array_list_data: ArrayList = file_helper.get_array_list()
        __huff_table: HuffTable = HuffTable(list_of_char=array_list_data).build_huff_data_map()
        __huff_tree: HuffTree = HuffTree(__huff_table)
        __huff_tree.build_huff_tree()
        coding_services: BinaryServices = BinaryServices(__huff_table)

        #### FOR THE UI ONLY
        self.view_helper.PrintHashTable(__huff_table)

        ###### converting every char of the text into a 0 and 1
        encoded_binary_string = coding_services.convert_text_to_binary(array_list_data)
        ####convert everting into utf8 and encode
        encoded_table = coding_services.convert_table_to_char()
        len_table = chr(coding_services.get_len_of_table())
        missing_binary = chr(coding_services.get_len_missing_binary())
        final_converted_string = len_table + missing_binary + encoded_table + encoded_binary_string
        self.out_file.write(final_converted_string.encode(encoding="utf-8", errors="strict"))








    def decode(self):
        file_helper = FileHelper(self.in_file,"utf8")
        file_helper.fetch_symbols()
        binary_string = file_helper.get_string_byte()
        __huff_table = HuffTable(list_of_huff_data=file_helper.get_array_list()).build_huff_data_map()
        #### FOR THE UI ONLY
        self.view_helper.PrintHashTable(__huff_table)
        ######
        __huff_tree = HuffTree(__huff_table)
        __huff_tree.build_huff_tree_from_huff_code()
        self.out_file.write(bytearray(__huff_tree.convert_binary_code(binary_string).encode(encoding="utf-8")))