from Util.GenericCollections.ArrayList import ArrayList


class ArrayQueue(object):

    def __init__(self, capacity: int = 1000000):
        self.__capacity = capacity
        self.__front = 0
        self.__rear = 0
        self.__items = ArrayList(capacity)

    def enqueue(self, item):
        if self.__capacity == self.__rear:
            print('Queue is full.')
            return
        self.__items.add_back(item)
        self.__rear += 1

    def shift_left(self, index):
        for i in range(index, self.__rear - 1):
            self.__items.put(self.__items[i + 1],i)

    def dequeue(self):
        if self.__front>=self.__rear:
            print('Queue is empty')
            return None
        to_return = self.__items[self.__front]
        self.__front += 1
        return to_return

    def peek(self):
        return self.__items[self.__front]

    def empty(self):
        return self.__front>=self.__rear

    def __str__(self):
        to_return = "["
        for i in range(self.__rear):
            to_return = to_return + str(self.__items[i])
            if i < self.__rear - 1:
                to_return = to_return + ','
        return to_return + ']'

