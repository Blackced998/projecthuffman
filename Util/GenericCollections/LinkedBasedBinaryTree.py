from Model.HuffNode import HuffNode



class LinkedBinaryTree(object):

    def __init__(self, root=None):
        self._root: HuffNode = root
        self._size = 0


    def add(self, symbol, frequency):
        new_node: HuffNode = HuffNode(symbol, frequency)
        self._size += 1
        if self._root is None:
            print(f"{new_node.symbol} is the root")
            self._root = new_node
            return
        self.__attach(self._root, new_node)

    def add_root(self,root):
        self._root=root


    def __attach(self, root, new_node):
        if root is not None:
            if root.__gt__(new_node):
                if root.left_child:
                    self.__attach(root.left_child, new_node)
                else:
                    root.left_child = new_node
                    print(f"{new_node.symbol} got added in the left child of {root}")
            elif root.__le__(new_node):
                if root.right_child:
                    self.__attach(root.right_child, new_node)
                else:
                    root.right_child = new_node
                    print(f"{new_node.symbol} got added in the right child of {root}")

    def remove(self, key):
        pass

    def root(self):
        return self._root



