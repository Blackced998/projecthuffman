from Util.GenericCollections.SinglyLinkedList import SinglyLinkedList
from Util.GenericCollections.SinglyLinkedList import Node
from Util.GenericCollections.ArrayList import ArrayList


class LinkedQueue(SinglyLinkedList):

    def enqueue(self, item):
        self.add_back(item)

    def dequeue(self):
        return self.remove_front()

    def peek(self):
        a: Node = self._head
        return a.item

    def empty(self):
        if self._head is None:
            return True
        return False