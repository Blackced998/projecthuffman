from Model.HuffData import HuffData

class SeparateChainingHashtable(object):
    def __init__(self, capacity=10):
        self.storage = [[] for _ in range(capacity)]
        self.capacity = capacity
        self.length = 0

    def __setitem__(self, key, value: HuffData):
        hashed_key = hash(key)
        storage_index = hashed_key % self.capacity
        for element in self.storage[storage_index]:
            if key == element[0]:
                element[1] = value
                break
        else:
            self.storage[storage_index].append([key,value])
            self.length += 1

    def put(self, key, value: HuffData):
        self.__setitem__(key, value)

    def remove(self, key):
        self.__delitem__(key)

    def __delitem__(self, key):
        index = self._find_item(key)
        self.storage[index] = None

    def __getitem__(self, key):
        hashed_key = hash(key)
        index = hashed_key % self.capacity
        for i in self.storage[index]:
            if i[0] == key:
                return i[1]
        raise KeyError(f'Key {key} not found')

    def __len__(self):
        return self.length

    def __iterate_kv(self):
        for sub_list in self.storage:
            if not sub_list:
                continue
            for element in sub_list:
                yield element

    def __iter__(self):
        for key_value in self.__iterate_kv():
            yield key_value[0]

    def keys(self):
        return self.__iter__()

    def values(self):
        for key_value in self.__iterate_kv():
            yield key_value[1]

    def items(self):
        return self.__iterate_kv()

    def get(self, key):
        try:
            return self.__getitem__(key)
        except KeyError:
            return None

    def __str__(self):
        result = []
        for element in self.storage:
            for key_value in element:
                if isinstance(key_value[0], str):
                    key_str = '\'{}\''.format(key_value[0])
                else:
                    key_str = '{}'.format(key_value[0])
                if isinstance(key_value[1], str):
                    value_str = '\'{}\''.format(key_value[1])
                else:
                    value_str = '{}'.format(key_value[1])
                result.append('{}: {}'.format(key_str, value_str))
        key_value_pairs_str = '{}'.format(', '.join(result))
        return '{' + key_value_pairs_str + '}'

    def __repr__(self):
        return self.__str__()


