from Util.GenericCollections.ArrayList import ArrayList


class SortedList(object):
    def __init__(self, data=None):
        self.__items = ArrayList()
        if data is not None:
            for item in data:
                self.add(item)

    def __getitem__(self, index):
        return self.__items.get(index)

    def size(self):
        return self.__items.length

    def __len__(self):
        return self.size()

    length = __len__

    def add(self, item):

        if self.size() == 0:
            self.__items.add_back(item)
        else:
            index = self.__add_by_comparing(item)
            if index == self.size():
                self.__items.add_back(item)
            else:
                self.__items.insert_at(item=item,index = index )

    def __add_by_comparing(self, item):
        for i in range( 0, self.size(), 1):
            if self.__items[i].__lt__(item):
                return i
        return self.size()

    def find(self, item):
        return self.__find_item_binary_search(item, 0, self.size() - 1)

    def __find_item_binary_search(self, item, start, end):
        middle: int
        middle = (start + end) // 2
        if not start > end:
            if self.__items.get(index=middle) == item:
                return middle
            if self.__items.get(index=middle) < item:
                return self.__find_item_binary_search(item, middle + 1, end)
            if self.__items.get(index=middle) > item:
                return self.__find_item_binary_search(item, start, middle - 1)
        else:
            return -1



    def remove(self, index):
        return self.__items.remove(index)

    def remove_back(self):
        return self.__items.remove_back()

    def remove_front(self):
        return self.__items.remove_front()

    def get(self, index):
        return self.__items.get(index)

    def is_empty(self) -> bool:
        return self.__items.is_empty()


    def __iter__(self):
        for item in self.__items:
            yield item

    def __str__(self):
        return str(self.__items)


