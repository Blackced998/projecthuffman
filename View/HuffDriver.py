import os

from Service.HuffmanCodecEngine import HuffmanCodecEngine

class HuffDriver:


    def encode(in_file, out_file):
        codecEngine = HuffmanCodecEngine(in_file, out_file)
        codecEngine.encode()


    def decode(in_file, out_file):
        codecEngine = HuffmanCodecEngine(in_file, out_file)
        codecEngine.decode()

    if __name__ == "__main__":
        while 1:
            print("------------------------------------HuffNonBinary Project-----------------------------------------------")
            print("Veuillez choisir si vous souhaitez encoder (1) ou décoder (2)")
            choice = int(input("1 ou 2 : "))

            print("Veuillez indiquer le fichier source ainsi que le fichier de destination (touche entrée pour le choix par défaut)")

            if choice == 1:
                source = input("Source (default = ../mydocument.txt) : ")
                dest = input("Destination (default = ../myencodeddocument.txt) : ")
                if source == "":
                    source = "../mydocument.txt"
                if dest == "":
                    dest = "../myencodeddocument.txt"
                print("Encodage en cours")
                encode(source, dest)
                sourceSize = os.path.getsize(source)
                destSize = os.path.getsize(dest)
                print("Taille du fichier source : " + str('%.1f'%(sourceSize / 1000)) + " KB")
                print("Taille du fichier encodé : " + str('%.1f'%(destSize / 1000)) + " KB")
                print("Taux de compression : " + str('%.2f'%(100 - destSize / sourceSize * 100)) + "%")
            elif choice == 2:
                source = input("Source (default : ../myencodeddocument.txt) : ")
                dest = input("Destination (default : ../mydecodeddocument.txt) : ")
                if source == "":
                    source = "../myencodeddocument.txt"
                if dest == "":
                    dest = "../mydecodeddocument.txt"
                print("Décodage en cours")
                decode(source, dest)
            else:
                print("Veuillez choisir des choix valides")
