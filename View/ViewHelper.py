from texttable import Texttable
from Model.HuffTable import HuffTable
class ViewHelper:


    def PrintHashTable(self,table: HuffTable):
        t = Texttable()
        t.set_cols_dtype(['t',  # text
                          'i',
                          't',
                          ])
        t.set_cols_align(["l", "l", "r"])
        t.add_row(["key", "frequency", "huff_code"])

        for key in table.keys():
            current = table.get(key)
            if key == "\n":
                t.add_row(["\\n", current.frequency, current.huff_code])
            else:
                t.add_row([key, current.frequency, current.huff_code])
        print(t.draw())